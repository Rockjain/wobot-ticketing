import React from 'react';
import { TouchableOpacity, Image} from 'react-native';
import { createStackNavigator, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";
import Colors from '../Config/Colors';

import SplashScreen from '../Screens/InitialScreen/Splash';
import LoginScreen from '../Screens/InitialScreen/Login';
import ForgotScreen from "../Screens/InitialScreen/Forgot";

import Drawer from '../Drawer';

import HomeScreen from '../Screens/MainScreen/Home';
import NotificatoinScreen from '../Screens/MainScreen/Notification';
import TaskDetailScreen from '../Screens/MainScreen/TaskDetail';
import AddTicketScreen from '../Screens/MainScreen/AddTicket';
import ChangePasswordScreen from '../Screens/MainScreen/ChangePassword';
import TaskScreen from '../Screens/MainScreen/Tasks';
import ImageViewerScreen from '../Screens/MainScreen/ImageViewer';

const auth = createSwitchNavigator({
    Splash: SplashScreen,
    Login: LoginScreen,
    Forgot: ForgotScreen,
},{
    initialRouteName: 'Splash'

})

const homestack= createStackNavigator({
    Home: HomeScreen
},{
    headerMode: 'none'
})
const notificatoinstack= createStackNavigator({
    Notification: NotificatoinScreen
},{
    headerMode: 'none'
})

const TaskDetailstack= createStackNavigator({
    TaskDetail: TaskDetailScreen,
    ImageViewer: ImageViewerScreen
},{
    navigationOptions :({ navigation }) => ({
        headerTitle: 'Task Detail',
        drawerLabel: 'Task Detail',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.primary,
        },
        headerLeft: (
          <TouchableOpacity onPress={()=>navigation.navigate('Task')}>
          <Image style={{width: 22, height: 22, marginLeft: 20}} source={require('../Images/back.png')}></Image>
          </TouchableOpacity>
        ),
      })
})
const AddTicketstack= createStackNavigator({
    AddTask: AddTicketScreen
})
const TaskStack= createStackNavigator({
    Task: TaskScreen
},{
    headerMode: 'none'
})
const ChangePswrdstack= createStackNavigator({
    ChangePassword: ChangePasswordScreen
})

const main = createDrawerNavigator({
    Home: homestack,
    Notification: notificatoinstack,
    TaskDetail: TaskDetailstack,
    AddTicket: AddTicketstack,
    ChangePassword: ChangePswrdstack,
    Task: TaskStack
},{
    initialRouteName: 'Home',
    contentComponent: Drawer
})

export default Root = createSwitchNavigator({
      Auth: auth,
      Main: main,
},{
    initialRouteName: 'Auth'
})